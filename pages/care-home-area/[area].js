import PropTypes from "prop-types";
import dynamic from 'next/dynamic'

import Header from "@/components/Header";
import AreaSelector from "@/components/AreaSelector";
import Timeline, { getTotalAverage } from "@/components/Timeline";
import { format, parse } from "date-fns";

import { Container } from "@/components/CommonStyles";

const ClientChart = dynamic(() => import('@/components/BarChart'), {
  ssr: false
});

export default function Region(props) {
  const {
    buildDate,
    fileDate,
    jsonUrl,
    total,
    totals = [],
    area,
    areas = []
  } = props;

  const dateList = totals.map(item => item.date).reverse().slice(0, totals.length - 4);

  const data = dateList.map(date => {
    const row = {
      date: format(parse(date, "yyyy-MM-dd", new Date()), "dd/MM/yyyy"),
    };

    const dateIndex = totals.findIndex(item => item.date === date);
    const total3 = getTotalAverage(totals, dateIndex + 1, 3);

    row["Three Day Average"] = Math.round(total3);
    row.Deaths = totals[dateIndex].value;

    return row;
  });

  return (
    <Container>
    <Header
        header={`Area: ${area}`}
        subHeader="Care Homes and Outside Hospital COVID-19 Daily Deaths"
        careHomes
        buildDate={buildDate}
        fileDate={fileDate}
        jsonUrl={jsonUrl}
      />
      <AreaSelector areas={areas} defaultValue={area} />
      <p>The below chart displays the deaths per day</p>
      <ClientChart data={data} />
      <Timeline
        totals={totals}
        total={total}
      />
    </Container>
  );
}

Region.propTypes = {
  buildDate: PropTypes.string,
  fileDate: PropTypes.string,
  jsonUrl: PropTypes.string,
  totals: PropTypes.arrayOf(PropTypes.any),
  areas: PropTypes.arrayOf(PropTypes.any),
  total: PropTypes.number,
  area: PropTypes.string
};

export async function getStaticProps(ctx) {
  const format = require("date-fns/format");
  const loadData = require("@/services/loadData");
  const getSlug = require("@/services/getSlug").default;
  const data = await loadData();
  const buildDate = format(new Date(), "yyyy-MM-dd HH:mm:ss");

  const area = data.areaDetails.find((item) => {
    const slug = getSlug(item.area);
    return slug === ctx.params.area;
  });

  if (!area) {
    return {
      props: {},
    };
  }

  return {
    props: {
      buildDate,
      jsonUrl: data.jsonUrl,
      fileDate: data.fileDate,
      area: area.area,
      total: area.total,
      totals: area.deaths.reverse(),
      areas: data.areaDetails.map((item) => item.area),
    },
  };
}

export async function getStaticPaths() {
  const loadData = require("@/services/loadData");
  const getSlug = require("@/services/getSlug").default;
  const data = await loadData();

  return {
    paths: data.areaDetails.map((item) => {
      return "/care-home-area/" + getSlug(item.area);
    }),
    fallback: false,
  };
}
