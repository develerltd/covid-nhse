import PropTypes from "prop-types";
import dynamic from 'next/dynamic'

import Header from "@/components/Header";
import TrustSelector from "@/components/TrustSelector";
import RegionSelector from "@/components/RegionSelector";
import Timeline, { getTotalAverage } from "@/components/Timeline";
import { format, parse } from "date-fns";

import { Container } from "@/components/CommonStyles";

const ClientChart = dynamic(() => import('@/components/BarChart'), {
  ssr: false
});

export default function Region(props) {
  const {
    buildDate,
    fileDate,
    jsonUrl,
    totalAdjustment,
    total,
    adjustments = [],
    totals = [],
    trusts = [],
    regions = [],
    region
  } = props;

  const dateList = totals.map(item => item.date).reverse().slice(0, totals.length - 4);

  let runningTotal = 0;
  const data = dateList.map(date => {
    const row = {
      date: format(parse(date, "yyyy-MM-dd", new Date()), "dd/MM/yyyy"),
    };

    const dateIndex = totals.findIndex(item => item.date === date);
    const total3 = getTotalAverage(totals, dateIndex, 3);

    row["Three Day Average"] = Math.round(total3);
    row.Deaths = totals[dateIndex].value;

    runningTotal+= row.Deaths;

    if (runningTotal === 0) {
      return null;
    }

    return row;
  }).filter(Boolean);

  return (
    <Container>
      <Header
        header={`Region: ${region}`}
        subHeader="NHS England COVID-19 Daily Deaths"
        buildDate={buildDate}
        fileDate={fileDate}
        jsonUrl={jsonUrl}
      />
      <TrustSelector label={`${region} based trusts`} trusts={trusts} />
      <RegionSelector regions={regions} defaultValue={region} />
      <p>The below chart displays the deaths per day, from date of first death, with the three day average. Owing to the inaccuracy of the most recent days, these have been omitted to better show the shape of the data.</p>
      <ClientChart data={data} areas={regions} />
      <Timeline
        totals={totals}
        adjustments={adjustments}
        total={total}
        totalAdjustment={totalAdjustment}
      />
    </Container>
  );
}

Region.propTypes = {
  buildDate: PropTypes.string,
  fileDate: PropTypes.string,
  jsonUrl: PropTypes.string,
  totalAdjustment: PropTypes.number,
  totals: PropTypes.arrayOf(PropTypes.any),
  trusts: PropTypes.arrayOf(PropTypes.any),
  regions: PropTypes.arrayOf(PropTypes.any),
  adjustments: PropTypes.arrayOf(PropTypes.any),
  total: PropTypes.number,
  region: PropTypes.string,
  name: PropTypes.string,
};

export async function getStaticProps(ctx) {
  const format = require("date-fns/format");
  const loadData = require("@/services/loadData");
  const getSlug = require("@/services/getSlug").default;
  const data = await loadData();
  const buildDate = format(new Date(), "yyyy-MM-dd HH:mm:ss");

  const region = data.regions.find((item) => {
    const slug = getSlug(item.region);
    return slug === ctx.params.region;
  });

  if (!region) {
    return {
      props: {},
    };
  }

  return {
    props: {
      buildDate,
      jsonUrl: data.jsonUrl,
      fileDate: data.fileDate,
      region: region.region,
      total: region.total,
      totals: region.deaths.reverse(),
      adjustments: region.adjustments,
      totalAdjustment: region.totalAdjustment,
      trusts: data.trusts.filter(item => item.region === region.region).map((item) => item.name),
      regions: data.regions.map((item) => item.region),
    },
  };
}

export async function getStaticPaths() {
  const loadData = require("@/services/loadData");
  const getSlug = require("@/services/getSlug").default;
  const data = await loadData();

  return {
    paths: data.regions.map((item) => {
      return "/region/" + getSlug(item.region);
    }),
    fallback: false,
  };
}
