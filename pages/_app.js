import App from "next/app";
import GlobalStyles from "@/components/GlobalStyles";

export default function MainApp(props) {
  return (
    <>
      <GlobalStyles />
      <App {...props} />
    </>
  );
}
