import PropTypes from "prop-types";
import dynamic from 'next/dynamic'

import Header from "@/components/Header";
import AreaSelector from "@/components/AreaSelector";
import Timeline, { getTotalAverage } from "@/components/Timeline";
import { Container } from "@/components/CommonStyles";
import { format, parse } from "date-fns";

const ClientChart = dynamic(() => import('@/components/Chart'), {
    ssr: false
  });
  

export default function Index(props) {
  const {
    buildDate,
    fileDate,
    jsonUrl,
    total,
    totals = [],
    areas = [],
    careHomes = [],
    otherLocation = []
  } = props;

  const dateList = totals.map(item => item.date).reverse();

  const data = dateList.map(date => {
    const row = {
      date: format(parse(date, "yyyy-MM-dd", new Date()), "dd/MM/yyyy"),
    };

    const careIndex = careHomes.findIndex(item => item.date === date);
    const otherIndex = otherLocation.findIndex(item => item.date === date);
    const dateIndex = totals.findIndex(item => item.date === date);
    const total7 = getTotalAverage(totals, dateIndex + 3, 7);

    row["Care Homes"] = careHomes[careIndex].value;
    row["Other Locations"] = otherLocation[otherIndex].value;
    row["Seven Day Average"] = Math.round(total7);
    row.Deaths = totals[dateIndex].value;

    return row;
  });

  return (
    <Container>
      <Header
        header="Care Homes and Outside Hospital COVID-19 Daily Deaths"
        careHomes
        buildDate={buildDate}
        fileDate={fileDate}
        jsonUrl={jsonUrl}
      />
      <AreaSelector areas={areas} />
      <p>The below chart displays the total deaths per day.</p>
      <ClientChart data={data} areas={["Care Homes", "Other Locations"]} />
      <Timeline
        totals={totals}
        total={total}
      />
    </Container>
  );
}

Index.propTypes = {
  buildDate: PropTypes.string,
  fileDate: PropTypes.string,
  jsonUrl: PropTypes.string,
  total: PropTypes.number,
  totals: PropTypes.arrayOf(PropTypes.any),
  careHomes: PropTypes.arrayOf(PropTypes.any),
  otherLocation: PropTypes.arrayOf(PropTypes.any),
  areas: PropTypes.arrayOf(PropTypes.any)
};

export async function getStaticProps() {
  const loadData = require("@/services/loadData");
  const format = require("date-fns/format");
  const data = await loadData();
  const buildDate = format(new Date(), "yyyy-MM-dd HH:mm:ss");

  const totals = data.careHomes.map((item, idx) => {
    const otherLocation = data.otherLocation.find(other => other.date === item.date);

    return {
        date: item.date,
        value: (item.value || 0) + (otherLocation.value || 0)
    }
  });

  const total = totals.reduce((acc, item) => acc + (item.value || 0), 0);

  return {
    props: {
      buildDate,
      jsonUrl: data.jsonUrl,
      fileDate: data.careFileDate,
      totals: [...totals].reverse(),
      careHomes: [...data.careHomes].reverse(),
      otherLocation: [...data.otherLocation].reverse(),
      total,
      areas: data.areaDetails.map((item) => item.area),
    },
  };
}
