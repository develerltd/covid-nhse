import PropTypes from "prop-types";
import dynamic from 'next/dynamic'

import Header from "@/components/Header";
import TrustSelector from "@/components/TrustSelector";
import RegionSelector from "@/components/RegionSelector";
import Timeline, { getTotalAverage } from "@/components/Timeline";
import { Container } from "@/components/CommonStyles";
import { format, parse } from "date-fns";

const ClientChart = dynamic(() => import('@/components/Chart'), {
  ssr: false
});


export default function Index(props) {
  const {
    buildDate,
    fileDate,
    jsonUrl,
    totalAdjustment,
    total,
    totals = [],
    adjustments = [],
    trusts = [],
    regions = [],
  } = props;

  const dateList = totals.map(item => item.date).reverse().slice(0, totals.length - 4);

  const data = dateList.map(date => {
    const row = {
      date: format(parse(date, "yyyy-MM-dd", new Date()), "dd/MM/yyyy"),
    };

    const dateIndex = totals.findIndex(item => item.date === date);
    const total3 = getTotalAverage(totals, dateIndex, 3);

    row["Three Day Average"] = Math.round(total3);

    props.regionData.map(region => {
      const dayData = region.deaths.find(item => item.date === date);

      row[region.region] = dayData.value;
    });

    return row;
  });

  return (
    <Container>
      <Header
        header="NHS England COVID-19 Total Daily Deaths"
        buildDate={buildDate}
        fileDate={fileDate}
        jsonUrl={jsonUrl}
      />
      <TrustSelector trusts={trusts} />
      <RegionSelector regions={regions} />
      <p>The below chart displays the total deaths per day, stacked by region in which they occured. Owing to the inaccuracy of the most recent days, these have been omitted to better show the shape of the data.</p>
      <ClientChart data={data} areas={regions} />
      <Timeline
        totals={totals}
        adjustments={adjustments}
        total={total}
        totalAdjustment={totalAdjustment}
      />
    </Container>
  );
}

Index.propTypes = {
  buildDate: PropTypes.string,
  fileDate: PropTypes.string,
  jsonUrl: PropTypes.string,
  total: PropTypes.number,
  totalAdjustment: PropTypes.number,
  totals: PropTypes.arrayOf(PropTypes.any),
  trusts: PropTypes.arrayOf(PropTypes.any),
  regions: PropTypes.arrayOf(PropTypes.any),
  adjustments: PropTypes.arrayOf(PropTypes.any),
  regionData: PropTypes.arrayOf(PropTypes.any)
};

export async function getStaticProps() {
  const loadData = require("@/services/loadData");
  const format = require("date-fns/format");
  const data = await loadData();
  const buildDate = format(new Date(), "yyyy-MM-dd HH:mm:ss");

  return {
    props: {
      buildDate,
      jsonUrl: data.jsonUrl,
      fileDate: data.fileDate,
      totals: data.totals.reverse(),
      total: data.total,
      totalAdjustment: data.totalAdjustment,
      adjustments: data.adjustments,
      trusts: data.trusts.map((item) => item.name),
      regions: data.regions.map((item) => item.region),
      regionData: data.regions.sort((a, b) => {
        if (a.total > b.total) {
          return -1;
        }

        if (a.total < b.total) {
          return 1;
        }

        return 0;
      })
    },
  };
}
