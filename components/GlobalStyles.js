import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    html, body {
        font-size: 16px;
        min-height: 100vh;
        padding: 0;
        margin: 0;
        box-sizing: border-box;
    }

    header, footer, nav, div, span, p, h1, h2, h3, h4, h5, h6, table, tbody, tr, td, th, input, select, textarea, button, ul, li {
        box-sizing: border-box;
    }
    
    p {
        font-family: "Roboto";
    }
`;
