import PropTypes from "prop-types";

import Router from "next/router";

import getSlug from "@/services/getSlug";
import { Wrapper } from "./TrustSelector";

function handleNavigate(e) {
  const region = e.target.value;
  const slug = getSlug(region);
  Router.push(`/care-home-area/${slug}`);
}

export default function AreaSelector(props) {
  const { areas = [], defaultValue } = props;
 
  const areaList = areas.map((name) => (
    <option key={name} value={name}>
      {name}
    </option>
  ));

  return (
    <Wrapper>
      <label htmlFor="areaSelect">Select an area to see specific details: </label>
      <select onChange={handleNavigate} id="areaSelect" defaultValue={defaultValue}>
        <option>-- SELECT AN AREA --</option>
        {areaList}
      </select>
    </Wrapper>
  );
}

AreaSelector.propTypes = {
  regions: PropTypes.arrayOf(PropTypes.any),
};
