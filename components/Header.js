import PropTypes from "prop-types";
import styled from "styled-components";
import Link from "next/link";
import Head from "next/head";
import getSlug from "@/services/getSlug";

const Wrapper = styled.header`
  margin: 0;
  margin-top: 20px;
  font-family: "Roboto";
  display: flex;
  flex-direction: column;

  h1,
  h2 {
    padding: 0px 14px;
    margin: 0;
    margin-bottom: 14px;
  }

  h1 {
    font-size: 28px;
  }
`;

export const Notice = styled.div`
  background-color: #ffffdd;
  padding: 8px 14px;
  margin-top: 14px;
  p {
    word-wrap: break-word;
    margin: 0;
    line-height: 2em;
  }
`;

const Download = styled.a`
  display: inline-block;
  font-size: 14px;
  background-color: #ddd;
  padding: 12px;
  margin: auto;

  text-decoration: none;
  color: black;
  font-weight: bold;
`;

const DateTag = styled.p`
  background-color: #eeffff;
  font-size: 14px;
  padding: 8px;
  margin-left: 16px;
`;

export default function Header(props) {
  const { jsonUrl, header, subHeader, region, buildDate, fileDate, careHomes } = props;
  const regionLink = region && `/region/${getSlug(region)}`;

  return (
    <Wrapper>
      <Head>
        <title>{header} | NHS England COVID-19 Deaths by Date Of Death</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content={header} />
        <meta property="og:type" content="website" />
        <meta name="og:title" property="og:title" content={header} />
        <meta
          property="og:site_name"
          content="NHS England COVID-19 Daily Deaths"
        />
        <meta property="og:url" content="http://covid-nhse.develer.co.uk/" />
        <link
          rel="apple-touch-icon-precomposed"
          href="/apple-touch-icon-precomposed.png"
        />
        <link rel="icon" type="image/png" href="/favicon.png" />
      </Head>

      <h1>{header}</h1>
      <DateTag>
        <strong>Site Last Built At:</strong> {buildDate}
      </DateTag>
      <DateTag>
        <strong>Document retrieved from NHS England At: </strong> {fileDate}
      </DateTag>
      {(
        <h2>
          {subHeader ? `${subHeader} | ` : ''}
          <Link href="/">
            <a>NHS England Hospitals</a>
          </Link> | {" "}
          <Link href="/care">
            <a>Care Homes</a>
          </Link>
        </h2>
      )}
      {region && <h2><Link href={regionLink}><a>{region}</a></Link></h2>}
      {!careHomes && <Notice>
        <p>
          All deaths are recorded against the date of death rather than the date
          the deaths were announced.
        </p>
        <p>
          Original Source of Data:
          <br />
          <a
            href="https://www.england.nhs.uk/statistics/statistical-work-areas/covid-19-daily-deaths/"
            rel="noopener noreferer"
          >
            https://www.england.nhs.uk/statistics/statistical-work-areas/covid-19-daily-deaths/
          </a>
        </p>
        <p>
          The smaller numbers next to the total show the adjustment to the
          figure from the daily update.
        </p>
        <p>
          <strong>
            Figures particularly for recent days are likely to be updated in
            future releases.
          </strong>
        </p>
      </Notice>}
      {careHomes && <Notice>
        <p>
          Original Source of Data:
          <br />
          <a
            href="https://www.ons.gov.uk/file?uri=/peoplepopulationandcommunity/birthsdeathsandmarriages/deaths/datasets/numberofdeathsincarehomesnotifiedtothecarequalitycommissionengland/2020/cqcreferencetables3.xlsx"
            rel="noopener noreferer"
          >
            https://www.ons.gov.uk/.../cqcreferencetables3.xlsx
          </a>
        </p>
      </Notice>
      }
      <Download href={jsonUrl} download={jsonUrl} target="_blank">
        Click here to download the JSON document used to generate this site
      </Download>
    </Wrapper>
  );
}

Header.propTypes = {
  header: PropTypes.string,
  subHeader: PropTypes.string,
  region: PropTypes.string,
  buildDate: PropTypes.string,
  fileDate: PropTypes.string,
  jsonUrl: PropTypes.string,
};
