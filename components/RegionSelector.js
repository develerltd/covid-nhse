import PropTypes from "prop-types";

import Router from "next/router";

import getSlug from "@/services/getSlug";
import { Wrapper } from "./TrustSelector";

function handleNavigate(e) {
  const region = e.target.value;
  const slug = getSlug(region);
  Router.push(`/region/${slug}`);
}

export default function RegionSelector(props) {
  const { regions = [], defaultValue } = props;
 
  const regionList = regions.map((name) => (
    <option key={name} value={name}>
      {name}
    </option>
  ));

  return (
    <Wrapper>
      <label htmlFor="regionSelect">Select a region to see specific details: </label>
      <select onChange={handleNavigate} id="regionSelect" defaultValue={defaultValue}>
        <option>-- SELECT A REGION --</option>
        {regionList}
      </select>
    </Wrapper>
  );
}

RegionSelector.propTypes = {
  regions: PropTypes.arrayOf(PropTypes.any),
};
