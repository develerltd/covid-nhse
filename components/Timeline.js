import PropTypes from "prop-types";
import styled from "styled-components";
import { format, parse } from "date-fns";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #f6f6f6;
  min-height: 100%;
  max-width: 900px;
  margin: auto;

  h3:first-child {
    text-align: center;
    font-family: "Roboto";
    padding-top: 14px;
    margin: 0;
    padding-bottom: 7px;
  }
`;

const PeakDays = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  justify-content: space-around;
  padding-bottom: 14px;
`;

const PeakDay = styled.div`
  background-color: rgba(255, 255, 255, 0.4);
  padding: 8px 8px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  text-align: center;
  font-family: "Roboto";
  border: 1px solid #555;
`;

const PeakDate = styled.div`
  font-weight: bold;
  font-size: 12px;
`;
const PeakValue = styled.div`
  font-weight: bold;
  font-size: 24px;
`;

export const DateEntry = styled.h3`
  padding: 0;
  width: 120px;
  font-size: 18px;
  height: 40px;
  line-height: 40px;
  font-family: "Roboto";
  text-align: center;
  background-color: white;
  border-radius: 40px;
  position: absolute;
  top: calc(50% - 35px);
  background: linear-gradient(
    180deg,
    rgba(246, 246, 246, 1) 0%,
    rgba(255, 254, 228, 1) 34%,
    rgba(255, 255, 255, 1) 63%,
    rgba(207, 238, 244, 1) 84%,
    rgba(246, 246, 246, 1) 94%
  );
`;

export const Count = styled.h2`
  margin: 20px;
  margin-top: auto;
  margin-bottom: auto;
  font-size: 48px;
  font-family: "Roboto";
  text-align: center;

  ${(props) =>
    props.tentative &&
    `
  color: #66A;`}
`;

export const Adjustment = styled.div`
  font-size: 18px;
  color: #222;
  display: block;
  vertical-align: middle;
  margin-left: 12px;
  font-family: "Roboto";
  text-align: center;
  background-color: white;
  padding: 8px;

  @media screen and (min-width: 768px) {
    display: inline-block;
  }
`;

export const DayAvg = styled.div`
  font-size: 14px;
  font-family: "Roboto";
  text-align: center;
`;

export const TimelineEntry = styled.div`
  display: flex;
  position: relative;
  width: 50%;
  min-height: 140px;

  &:nth-child(odd) {
    border-left: 1px solid black;
    flex-direction: row-reverse;
    margin-left: auto;

    ${Count} {
      margin-left: auto;
    }

    ${DateEntry} {
      left: -60px;
    }
  }

  &:nth-child(even) {
    border-right: 1px solid black;
    flex-direction: row;
    transform: translateX(1px);
    margin-right: auto;

    ${DateEntry} {
      right: -59px;
    }
  }

  @media screen and (min-width: 768px) {
    ${Count} {
      margin: auto;
    }
  }
`;

const Total = styled.h3`
  margin: auto;
  font-family: "Roboto";
  font-size: 28px;
  text-align: center;
  padding-top: 14px;
  padding-bottom: 14px;
  border-bottom: 1px solid black;
`;

const TotalAdjustment = styled.div`
  display: inline-block;
  font-size: 14px;
  color: #333;
  vertical-align: middle;
  background-color: white;
  padding: 8px;
`;

export function getTotalAverage(list, idx, period) {
  const firstPart = Math.floor(period / 2);
  const lastPart = Math.ceil(period / 2);
  return Math.round(
    list.slice(idx - firstPart, idx + lastPart).reduce((acc, item) => acc + item.value, 0) /
      period
  );
}

export default function Timeline(props) {
  const { totalAdjustment, totals = [], adjustments = [], total } = props;

  const newTotals = [...totals];
  const peakDays = newTotals
    .sort((t1, t2) => {
      if (t1.value > t2.value) {
        return -1;
      }

      if (t1.value < t2.value) {
        return 1;
      }

      return 0;
    })
    .slice(0, 3);

  const peakDaysList = peakDays.map((item) => {
    const realDate = parse(item.date, "yyyy-MM-dd", new Date());
    const adjustmentRow = adjustments.find(
      (row) => row.date === item.date && row.value !== 0
    );
    const adjustmentSign =
      adjustmentRow && (adjustmentRow.value > 0 ? "+" : "-");

    return (
      <PeakDay key={item.date}>
        <PeakDate>{format(realDate, "dd/MM/yyyy")}</PeakDate>
        <PeakValue>
          {item.value}
          {adjustmentRow && (
            <Adjustment>
              {adjustmentSign} {adjustmentRow.value}
            </Adjustment>
          )}
        </PeakValue>
      </PeakDay>
    );
  });

  const timeList = totals.map((item, idx) => {
    const tda3 = getTotalAverage(totals, idx, 3);
    const tda5 = getTotalAverage(totals, idx, 5);
    const realDate = parse(item.date, "yyyy-MM-dd", new Date());

    const formattedDate = format(realDate, "dd/MM/yyyy");

    const adjustmentRow = adjustments.find(
      (row) => row.date === item.date && row.value !== 0
    );
    const adjustmentSign =
      adjustmentRow && (adjustmentRow.value > 0 ? "+" : "-");

    return (
      <TimelineEntry key={item.date}>
        <Count tentative={idx < 6}>
          {item.value}
          {adjustmentRow && (
            <Adjustment>
              {adjustmentSign} {adjustmentRow.value}
            </Adjustment>
          )}
          {idx > 5 && <DayAvg>3 Day Avg {tda3}</DayAvg>}
          {idx > 3 && <DayAvg>5 Day Avg {tda5}</DayAvg>}
        </Count>

        <DateEntry>{formattedDate}</DateEntry>
      </TimelineEntry>
    );
  });

  const totalAdjustmentSign =
    totalAdjustment && (totalAdjustment > 0 ? "+" : "-");

  return (
    <Wrapper>
      <h3>Top Three Peak Days</h3>
      <PeakDays>{peakDaysList}</PeakDays>
      <Total>
        Total: {Number(total).toLocaleString()}{" "}
        {totalAdjustment !== 0 && (
          <TotalAdjustment>
            {totalAdjustmentSign} {totalAdjustment}
          </TotalAdjustment>
        )}
      </Total>

      {timeList}
    </Wrapper>
  );
}

Timeline.propTypes = {
  totalAdjustment: PropTypes.number,
  total: PropTypes.number,
  totals: PropTypes.arrayOf(PropTypes.any),
  adjustments: PropTypes.arrayOf(PropTypes.any),
};
