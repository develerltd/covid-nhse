import PropTypes from "prop-types";

import styled from "styled-components";
import Router from "next/router";

import getSlug from "@/services/getSlug";

export const Wrapper = styled.form`
  border-bottom: 1px solid #aaa;
  margin: auto;
  margin-bottom: 20px;
  margin-top: 20px;
  max-width: 900px;
  display: flex;
  padding: 20px;
  font-family: "Roboto";
  font-size: 18px;
  flex-direction: column;

  select {
    font-family: "Roboto";
    cursor: pointer;
    font-size: 14px;
    margin-top: 20px;
  }

  @media screen and (min-width: 768px) {
    flex-direction: row;

    label {
      width: 200px;
      margin-right: 20px;
    }

    select {
      margin-top: 0;
    }
  }
`;

function handleNavigate(e) {
  const trust = e.target.value;
  const slug = getSlug(trust);
  Router.push(`/${slug}`);
}

export default function TrustSelector(props) {
  const { trusts = [], label, defaultValue } = props;

  const trustList = trusts.map((name) => (
    <option key={name} value={name}>
      {name}
    </option>
  ));

  return (
    <Wrapper>
      <label htmlFor="trustSelect">{label || "Select a trust to see specific details:"}</label>
      <select onChange={handleNavigate} id="trustSelect" defaultValue={defaultValue}>
        <option>-- SELECT A TRUST --</option>
        {trustList}
      </select>
    </Wrapper>
  );
}

TrustSelector.propTypes = {
  trusts: PropTypes.arrayOf(PropTypes.any),
  label: PropTypes.string,
};
