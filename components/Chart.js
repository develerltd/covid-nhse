import {
    ComposedChart ,
  Area,
  Bar,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import styled from "styled-components";
import { useEffect, useState, useRef } from "react";

const colours = [
  {
    stroke: "#8884d8",
    fill: "#8884d8",
  },
  {
    stroke: "#82ca9d",
    fill: "#82ca9d",
  },
  {
    stroke: "#ffc658",
    fill: "#ffc658",
  },
  {
    stroke: "#dd6633",
    fill: "#dd6633",
  },
  {
    stroke: "#dd55dd",
    fill: "#dd55dd",
  },
  {
    stroke: "#dddd33",
    fill: "#dddd33",
  },
  {
    stroke: "#3377aa",
    fill: "#3377aa",
  },
];

const HiddenValue = styled.span`
  color: #aaa;
  cursor: pointer;
  font-family: "Roboto";
`;

const Value = styled.span`
  color: #000;
  cursor: pointer;
  font-family: "Roboto";
`;

const Wrapper = styled.div`
  position: absolute;
  left: 10px;
  right: 10px;
  max-width: 1024px;
  margin: auto;

  @media screen and (min-width: 768px) {
    left: 20px;
    right: 20px;
  }

  .xAxis {
    text.recharts-label {
      font-family: "Roboto";
    }
  }
`;

const WrapperContainer = styled.div`
  height: 480px;
  display: none;

  @media screen and (min-width: 568px) {
    height: 430px;
    display: block;
  }

  .recharts-legend-wrapper {
      left: 0px !important;
      width: 100% !important;
  }
`;

export default function Chart(props) {
  const [hiddenAreas, setHiddenAreas] = useState({});
  const [areaWidth, setAreaWidth] = useState(1024);
  const { data, areas } = props;
  const container = useRef();

  const handleClick = (clickData) => {
    const trimmedKey = clickData.dataKey.trim();

    if (clickData.payload.type !== "monotone") {
        return;
    }

    setHiddenAreas((oldAreas) => {
      return {
        ...oldAreas,
        [trimmedKey]: !oldAreas[trimmedKey],
      };
    });
  };

  const formatter = (value) => {
    const trimmedValue = value.trim();

    if (hiddenAreas[trimmedValue]) {
      return <HiddenValue>{trimmedValue}</HiddenValue>;
    }

    return <Value>{trimmedValue}</Value>;
  };

  const areaList = props.areas.map((item, idx) => {
    const colour = colours[idx % colours.length];
    let realDataKey = item;

    if (hiddenAreas[item]) {
      realDataKey = `${item} `;
    }

    return (
      <Bar
        isAnimationActive={false}
        key={item}
        type="monotone"
        dataKey={realDataKey}
        stackId="1"
        stroke={colour.stroke}
        fill={colour.fill}
      />
    );
  });

  useEffect(() => {
    function handleResize() {
      const { width } = container.current.getBoundingClientRect();

      setAreaWidth(width);
    }

    window.addEventListener("resize", handleResize);

    handleResize();
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <WrapperContainer>
      <Wrapper ref={container}>
        <ComposedChart
          width={areaWidth}
          height={400}
          data={data}
          margin={
            areaWidth < 800
              ? {
                  top: 10,
                  right: 0,
                  left: -20,
                  bottom: 0,
                }
              : {
                  top: 10,
                  right: 20,
                  left: 0,
                  bottom: 0,
                }
          }
        >
          <CartesianGrid strokeDasharray="5 5" />
          <XAxis dataKey="date" />
          <YAxis />
          <Legend
            onClick={handleClick}
            formatter={formatter}
            verticalAlign={areaWidth < 800 ? "bottom" : "top"}
            height={36}
          />
          <Tooltip />
          {areaList}
          {data[0]["Three Day Average"] && <Line dataKey="Three Day Average" isAnimationActive={false} strokeWidth={4}  />}
          {data[0]["Seven Day Average"] && <Line dataKey="Seven Day Average" isAnimationActive={false} strokeWidth={4}  />}
        </ComposedChart>
      </Wrapper>
    </WrapperContainer>
  );
}
