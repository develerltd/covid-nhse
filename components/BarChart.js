import {
  ComposedChart,
  Bar,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import styled from "styled-components";
import { useEffect, useState, useRef } from "react";

const colours = [
  {
    stroke: "#8884d8",
    fill: "#8884d8",
  }
];

const Wrapper = styled.div`
  position: absolute;
  left: 10px;
  right: 10px;
  max-width: 1024px;
  margin: auto;

  @media screen and (min-width: 768px) {
    left: 20px;
    right: 20px;
  }

  .xAxis {
    text.recharts-label {
      font-family: "Roboto";
    }
  }
`;

const WrapperContainer = styled.div`
  height: 480px;
  display: none;

  @media screen and (min-width: 568px) {
    display: block;
    height: 430px;
  }

  .recharts-legend-wrapper {
    left: 0px !important;
    width: 100% !important;
  }
`;

export default function Chart(props) {
  const [areaWidth, setAreaWidth] = useState(1024);
  const { data } = props;
  const container = useRef();

  useEffect(() => {
    function handleResize() {
      const { width } = container.current.getBoundingClientRect();

      setAreaWidth(width);
    }

    window.addEventListener("resize", handleResize);

    handleResize();
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <WrapperContainer>
      <Wrapper ref={container}>
        <ComposedChart
          width={areaWidth}
          height={400}
          data={data}
          margin={
            areaWidth < 800
              ? {
                  top: 10,
                  right: 0,
                  left: -20,
                  bottom: 0,
                }
              : {
                  top: 10,
                  right: 20,
                  left: 0,
                  bottom: 0,
                }
          }
        >
          <CartesianGrid strokeDasharray="5 5" />
          <XAxis dataKey="date" />
          <YAxis />
          <Tooltip />
          <Bar
            isAnimationActive={false}
            type="monotone"
            dataKey="Deaths"
            stroke={colours[0].stroke}
            fill={colours[0].fill}
          />
          <Line
            dataKey="Three Day Average"
            isAnimationActive={false}
            strokeWidth={4}
          />
        </ComposedChart>
      </Wrapper>
    </WrapperContainer>
  );
}
