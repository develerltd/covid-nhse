const format = require("date-fns/format");
const xlsx = require("node-xlsx").default;
const fetch = require("isomorphic-unfetch");

function getDailyDocumentUrl() {
  const today = new Date();
  const fileDate = format(today, "d-MMMM-yyyy");
  const folderDate = format(today, "yyyy/MM");

  return `https://www.england.nhs.uk/statistics/wp-content/uploads/sites/2/${folderDate}/COVID-19-daily-announced-deaths-${fileDate}.xlsx`;
}

module.exports = async function getDailyDocument() {
  const url = getDailyDocumentUrl();

  const response = await fetch(url);
  if (!response.ok) {
    throw new Error("Error getting document");
  }

  const buffer = Buffer.from(await response.arrayBuffer());

  const document = await xlsx.parse(buffer);

  console.log("Downloaded Daily Document");
  return {
    dailyDeathsByRegion: document[2].data,
    dailyDeathsByTrust: document[5].data
  }
};
