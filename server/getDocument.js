const format = require("date-fns/format");
const xlsx = require("node-xlsx").default;
const fetch = require("isomorphic-unfetch");
const DocumentDate = require("./DocumentDate");

function getDocumentUrl() {
  const today = new Date();
  const fileDate = format(today, "d-MMMM-yyyy");
  const folderDate = format(today, "yyyy/MM");
  return `https://www.england.nhs.uk/statistics/wp-content/uploads/sites/2/${folderDate}/COVID-19-total-announced-deaths-${fileDate}.xlsx`;
}

module.exports = async function getDocument() {
  const url = getDocumentUrl();

  const response = await fetch(url);

  DocumentDate.set(format(
    new Date(response.headers.get("last-modified")),
    "yyyy-MM-dd HH:mm:ss"
  ))

  if (!response.ok) {
    throw new Error("Error getting document");
  }

  const buffer = Buffer.from(await response.arrayBuffer());

  const document = await xlsx.parse(buffer);

  console.log("Downloaded Document");
  return {
    deathsByTrust: document[5].data,
    deathsByRegion: document[2].data
  }
};
