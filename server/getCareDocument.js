const xlsx = require("node-xlsx").default;
const fetch = require("isomorphic-unfetch");
const addDays = require("date-fns/addDays");
const format = require("date-fns/format");
const parse = require("date-fns/parse");
const getDateRange = require("./getDateRange");
const getDeaths = require("./getDeaths");
const DocumentDate = require("./DocumentDate");

function getDocumentUrl() {
  return `https://www.ons.gov.uk/file?uri=/peoplepopulationandcommunity/birthsdeathsandmarriages/deaths/datasets/numberofdeathsincarehomesnotifiedtothecarequalitycommissionengland/2020/20200524officialsensitivecoviddeathnotificationsdata20200522.xlsx`;
//   return `https://www.ons.gov.uk/file?uri=/peoplepopulationandcommunity/birthsdeathsandmarriages/deaths/datasets/numberofdeathsincarehomesnotifiedtothecarequalitycommissionengland/2020/cqcreferencetables3.xlsx`;
}

const startDate = new Date("1900-01-01");

function getVerticalDateRange(table1) {
  const remaining = table1.slice(3);
  const end = remaining.findIndex((item) => item[0] === "All deaths");
  return remaining.slice(0, end).map((item) => {
    const value = item[0];

    if (typeof value === "number") {
      const newDate = addDays(startDate, value - 2);
      const utcTime = new Date(
        newDate.getTime() - newDate.getTimezoneOffset() * 60000
      );

      return format(utcTime, "yyyy-MM-dd");
    }

    const newDate = parse(`${value}0`, "dd/MM/yyyy", new Date());

    return format(newDate, "yyyy-MM-dd");
  });
}

module.exports = async function getDocument() {
  const url = getDocumentUrl();

  const response = await fetch(url);

  DocumentDate.setCare(
    "2020-04-28 12:00:00"
  );

  if (!response.ok) {
    throw new Error("Error getting document");
  }

  const buffer = Buffer.from(await response.arrayBuffer());

  const document = await xlsx.parse(buffer);

  const table1 = document[2].data;

  const dateRange = getVerticalDateRange(table1);

  const tableSlice = table1.slice(3);

  const careHomes = dateRange.map((date, idx) => {
    const value = tableSlice[idx][1];

    return {
      date,
      value,
    };
  });

  const otherLocation = dateRange.map((date, idx) => {
    const value = tableSlice[idx][2];

    return {
      date,
      value,
    };
  });

  const table2 = document[3].data;

  const areaDateRange = getDateRange(table2, 2, 1);

  console.log("Downloaded Care Document");

  const end = table2.findIndex(item => item[0] === "Notes:");

  const areaDetails = table2
    .slice(3, end)
    .map((item) => {
      const output = {
        area: item[0],
        deaths: getDeaths(item, areaDateRange, 1)
      };

      output.total = output.deaths.reduce((acc, item) => acc + item.value, 0)

      return output;
    })
    .sort((area1, area2) => {
      return area1.area.localeCompare(area2.area);
    });

  return {
    careHomes,
    otherLocation,
    areaDetails,
  };
};
