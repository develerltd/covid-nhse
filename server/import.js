const format = require("date-fns/format");
const fs = require("fs");
const path = require("path");
const processData = require("./processData");

const today = new Date();
const fileDate = format(today, "yyyy-MM-dd");

const filePath = path.resolve(
  path.dirname(path.resolve("package.json")),
  "public/data",
  `nhs-england-${fileDate}.json`
);

const latestFilePath = path.resolve(
  path.dirname(path.resolve("package.json")),
  "public/data",
  "latest.json"
);

processData().then((output) => {
  fs.writeFile(filePath, JSON.stringify(output), () => {
    console.log("Written");
  });
  fs.writeFile(latestFilePath, JSON.stringify(output), () => {
    console.log("Written");
  });
});
