module.exports = function getDeaths(row, dateRange, startColumn = 6) {
  const range = row.slice(startColumn, dateRange.length + startColumn);

  return range.map((value, idx) => ({
    date: dateRange[idx],
    value,
  }));
};
