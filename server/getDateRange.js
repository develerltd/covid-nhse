const addDays = require("date-fns/addDays");
const format = require("date-fns/format");

module.exports = function getDateRange(
  deathsByTrust,
  row = 15,
  startColumn = 6
) {
  const startDate = new Date("1900-01-01");
  const remaining = deathsByTrust[row].slice(startColumn);
  let end = remaining.findIndex((item) => !item);

  if (end === -1) {
    end = remaining.length;
  }
  
  const slice = remaining.slice(0, end);

  return slice.map((dateKey) => {
    const newDate = addDays(startDate, dateKey - 2);
    const utcTime = new Date(
      newDate.getTime() - newDate.getTimezoneOffset() * 60000
    );

    return format(utcTime, "yyyy-MM-dd");
  });
};
