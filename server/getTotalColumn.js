module.exports = function getTotalColumn(deathsByTrust, row = 15, startColumn = 6) {
    const remaining = deathsByTrust[row].slice(startColumn);
    const idx = remaining.findIndex((item) => item === "Total");
  
    return idx + startColumn;
  }
  
  