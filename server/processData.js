const getTotalColumn = require("./getTotalColumn");
const getDateRange = require("./getDateRange");
const getDeaths = require("./getDeaths");
const getDocument = require("./getDocument");
const getDailyDocument = require("./getDailyDocument");
const getCareDocument = require("./getCareDocument");
const DocumentDate = require("./DocumentDate");

function mapRegion(dailyChanges, totalIdx, dateRange) {
  return (item) => {
    const adjustment =
      dailyChanges.find((region) => region.region === item[1]) || {};

    return {
      region: item[1],
      total: item[totalIdx],
      deaths: getDeaths(item, dateRange, 4),
      adjustments: adjustment.deaths,
      totalAdjustment: adjustment.totalAdjustment,
    };
  };
}

function mapTrust(dailyChanges, totalIdx, dateRange) {
  return (item) => {
    const adjustment =
      dailyChanges.find((trust) => trust.name === item[4]) || {};

    if (!item[4]) {
      return null;
    }

    return {
      region: item[1],
      name: item[4],
      total: item[totalIdx],
      deaths: getDeaths(item, dateRange),
      adjustments: adjustment.deaths,
      totalAdjustment: adjustment.totalAdjustment,
    };
  };
}

module.exports = async function processData() {
  const { careHomes, otherLocation, areaDetails } = await getCareDocument();
  const { deathsByTrust, deathsByRegion } = await getDocument();
  const { dailyDeathsByRegion, dailyDeathsByTrust } = await getDailyDocument();

  const dateRange = getDateRange(deathsByTrust);
  const regionDateRange = getDateRange(deathsByRegion, 15, 4);
  const totalIdx = getTotalColumn(deathsByTrust);
  const regionTotalIdx = getTotalColumn(deathsByRegion, 15, 4);

  const dailyDateRange = getDateRange(dailyDeathsByTrust, 13, 5);
  const dailyRegionDateRange = getDateRange(dailyDeathsByRegion, 13, 3);
  const totalDailyIdx = getTotalColumn(dailyDeathsByTrust, 13, 5);
  const totalDailyRegionIdx = getTotalColumn(dailyDeathsByRegion, 13, 3);

  const dailyTrustChanges = dailyDeathsByTrust.slice(16).map((item) => ({
    name: item[4],
    totalAdjustment: item[totalDailyIdx],
    deaths: getDeaths(item, dailyDateRange, 5),
  }));

  const dailyRegionChanges = dailyDeathsByRegion.slice(16).map((item) => ({
    region: item[1],
    totalAdjustment: item[totalDailyRegionIdx],
    deaths: getDeaths(item, dailyRegionDateRange, 3),
  }));

  const trustDetails = deathsByTrust
    .slice(18)
    .map(mapTrust(dailyTrustChanges, totalIdx, dateRange))
    .filter(Boolean)
    .sort((trust1, trust2) => {
      return trust1.name.localeCompare(trust2.name);
    });

  const regionDetails = deathsByRegion
    .slice(18)
    .map(mapRegion(dailyRegionChanges, regionTotalIdx, regionDateRange))
    .filter(item => item.region)
    .sort((region1, region2) => {
      return region1.region.localeCompare(region2.region);
    });

  const totals = getDeaths(deathsByTrust[16], dateRange);
  const adjustments = getDeaths(dailyDeathsByTrust[14], dailyDateRange, 5);

  return {
    fileDate: DocumentDate.get(),
    careFileDate: DocumentDate.getCare(),
    trusts: trustDetails,
    regions: regionDetails,
    total: deathsByTrust[16][totalIdx],
    totalAdjustment: dailyDeathsByTrust[14][totalDailyIdx],
    totals,
    adjustments,
    careHomes,
    otherLocation,
    areaDetails
  };
};
