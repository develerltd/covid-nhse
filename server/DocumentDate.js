class DocumentDate {
    constructor() {
        this.date = null;
        this.careDate = null;
    }

    get() {
        return this.date;
    }

    getCare() {
        return this.careDate;
    }

    set(date) {
        this.date = date;
    }

    setCare(date) {
        this.careDate = date;
    }
}

module.exports = new DocumentDate();
