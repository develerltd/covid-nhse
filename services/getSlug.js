export default function getSlug(name) {
    return name.toLowerCase().replace(/[\W]+/g, "-");
}