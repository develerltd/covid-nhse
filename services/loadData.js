module.exports = async function loadData() {
  if (process.browser) {
    return null;
  }

  const format = require("date-fns/format");
  const today = new Date();

  const path = require("path");
  const filePath = path.resolve(`public/data/latest.json`);
  const fs = require("fs");

  return new Promise((resolve) => {
    fs.readFile(filePath, (err, result) => {
      const output = JSON.parse(result);
      resolve({
        ...output,
        jsonUrl: `/data/latest.json`
      });
    });
  });
};
